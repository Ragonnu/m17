﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuberias : MonoBehaviour
{

    //Creo una funcion, que cuando el pajaro colisiona, llama a la funcion Puntuacion().
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Pajaro> () != null)
        {
            GameController.instance.Puntuacion();
        }
    }
}
