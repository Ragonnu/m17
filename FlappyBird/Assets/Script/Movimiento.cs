﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    //Float donde pongo la velocidad de la escena. Negativo porque va hacia la izquierda.
    private float movimientoEscena = -1.5f;

    // Start is called before the first frame update
    void Start()
    {
        //Añado al RigidBody2D.velocity la variable creada con anterioridad como dato en x.
        GetComponent<Rigidbody2D>().velocity = new Vector2(movimientoEscena, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //Si el Boolean de la clase GameController esta en true (fin de juego), paro el movimiento de la pantalla.
        if (GameController.instance.gameOver == true)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
