﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pajaro : MonoBehaviour
{
    //Creo variable para añadirle la fuerza de salto.
	public int jumpForce;
    //Bandera que me dice si el paja ha muerto.
	public bool pajaroMuerto = false;

	// Start is called before the first frame update
	void Start()
    {

	}

	// Update is called once per frame
	void Update()
    {
        //Si clico el boton del raton añado la fuerza en eje y de salto y activo el trigger de "Aleteo".
		if (pajaroMuerto == false)
		{
			if(Input.GetMouseButtonDown (0))
			{
                this.GetComponent<Animator>().SetTrigger("Aleteo");
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
			}
		}
	}

	//Funcion que define las colisiones del pajaro, teniendo en cuenta si tiene escudo o no.
	void OnCollisionEnter2D(Collision2D coll)
    {
		if (coll.gameObject.tag == "Obstacle" || coll.gameObject.tag == "Tubo")
		{
            if (GameController.instance.escudoActivo != true)
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                GameController.instance.MuerePajaro();
                pajaroMuerto = true;
                this.GetComponent<Animator>().SetTrigger("Muerte");
            }
            else
            {
                GameObject.Destroy(coll.gameObject);
                GameController.instance.puntosEscudo = 0;
                GameController.instance.escudoActivo = false;
                GameController.instance.escudo.SetActive(false);
            }

            //GameObject.Destroy(this.gameObject);
        }
    }

	


}
