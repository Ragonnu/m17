﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    //Para referirme a mi player.
    public Pajaro player;
    //Tamaño de mi pantalla.
    public float size = 7.44f;

    //todos los prefabs disponibles
    public GameObject[] blockPrefabs;
    //puntero, apunta el final de los bloques generados
    private float puntero = 7f;
    //spawn, la variable extra que tiene que "mirar" el puntero para saber si generar un nuevo bloque
    private float spawn = 5;
    //Hago una variable tipo GameController.
    public static GameController instance;
    //Flag para marcar el fin del juego.
    public bool gameOver = false;
    //Creo variable tipo Text para la puntuacion.
    public Text scoreText;
    //Creo variable tipo GameObject para referirme al texto final.
    public GameObject gameOverText;
    //Entero para la puntuacion.
    public int score = 0;
     //Creo variable tipo GameObject para referirme al PowerUp escudo.
    public GameObject escudo;
    //Bandera que identifica si hay escudo activo.
    public bool escudoActivo = false;
    //Contador de puntos para activar escudo.
    public int puntosEscudo = 0;

    private void Awake()
    {
        //Creo un Singleton.
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        //justo al salir creamos un bloque inicial
        Instantiate(blockPrefabs[0]);
        //Asigno texto en el texto de score.
        scoreText.text = "Score: " + score;

    }

    // Update is called once per frame
    void Update()
    {
        //Si el juego a acabado y pulso clic del raton, reinicio el juego.
        if (gameOver == true && Input.GetMouseButton (0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }


        if (player != null && this.puntero > player.transform.position.x + spawn)
        {
            Debug.Log("el puntero esta en "+puntero);
            //instanciar hace aparecer en la escena activa
            GameObject newBlock = Instantiate(blockPrefabs[Random.Range(0, blockPrefabs.Length)]);
            //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
            //newBlock.transform.GetChild(0).transform.localScale.x;
            //ponemos la posicion del tamaño instanciado en la posicion del puntero + la mitad de su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
            newBlock.transform.position = new Vector2(puntero+size/2, 0);
            //aumentamos el puntero en el tamaño del bloque
            puntero += size;
        }
    }

    //Funcion para: Sumar puntos al texto Score, sumar puntos al contador escudo y accion de activar y desactivar escudo.
    public void Puntuacion ()
    {
        if (gameOver != true)
        {
            score++;
            puntosEscudo++;
            scoreText.text = "Score: " + score;
            puntero -= size;

            if (puntosEscudo >= 5)
            {
                Debug.Log("escudo activo " + puntosEscudo);
                escudo.SetActive(true);
                escudoActivo = true;
            }

        }
    }
    //Funcion para cuando muere el personaje.
    public void MuerePajaro ()
    {
        gameOverText.SetActive(true);
        gameOver = true;
    }

}
