﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepetirFondo : MonoBehaviour
{
    //Variable para el tamaño del suelo
    private float tamañoSuelo;

    // Start is called before the first frame update
    void Start()
    {
		tamañoSuelo = 7.44f;
        //tamañoSuelo = GetComponent<BoxCollider2D>().size.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < -tamañoSuelo)
        {
            ReponerFondo();
        }
    }
    //Funcion que cada x distancia me repone el fondo.
    private void ReponerFondo()
    {
        Vector2 referencia = new Vector2(tamañoSuelo * 2f, 0);
        transform.position = (Vector2)transform.position + referencia;
    }
}
