﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prueba : MonoBehaviour
{
    public int vel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("d") || Input.GetKey("D"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));
        }
        if (Input.GetKey("a") || Input.GetKey("A"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, vel));
        }
    }
}
